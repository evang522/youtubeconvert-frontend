This program requires the installation of ffmpeg.

Windows Install:
https://github.com/adaptlearning/adapt_authoring/wiki/Installing-FFmpeg

Mac Install: 
http://www.renevolution.com/ffmpeg/2013/03/16/how-to-install-ffmpeg-on-mac-os-x.html

Debian Linux Install:
sudo apt get install ffmpeg