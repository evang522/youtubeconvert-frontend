//================================== Import Dependencies ====================>
import axios from 'axios';
import {YOUTUBE_API_URL, YOUTUBE_API_KEY} from '../../config';


//================================== Sync Actions ====================>

export const SET_SEARCH_TERM = 'SET_SEARCH_TERM';
export const setSearchTerm = searchTerm => ({
  type:SET_SEARCH_TERM,
  searchTerm
})



export const STORE_VIDEO_LIST = 'STORE_VIDEO_LIST';
export const storeVideoList = videoList => ({
  type:STORE_VIDEO_LIST,
  videoList
})


export const ADD_SONG_TO_LIST = 'ADD_SONG_TO_LIST';
export const addSongToList = song => ({
  type:ADD_SONG_TO_LIST,
  song
})

export const CLEAR_DOWNLOAD_LIST = 'CLEAR_DOWNLOAD_LIST';
export const clearDownloadLIst = () => ({
  type:CLEAR_DOWNLOAD_LIST
})

//================================== Async Actions ====================>


export const getVideoResults = () => (dispatch,getState) => {
  const searchTerm = getState().general.searchTerm;
  let results;
  axios({
    url:`${YOUTUBE_API_URL}?key=${YOUTUBE_API_KEY}&maxResults=24&part=snippet&q=${searchTerm}`,
    method:'GET'
  })
  .then(response => {
    results = response.data.items.filter(item => {
      return item.id.kind === 'youtube#video'
    })
    dispatch(storeVideoList(results));
  })
}


export const requestDownload = () => (dispatch,getState) => {
  const downloadList = getState().general.downloadList;
  dispatch(clearDownloadLIst());
  axios({
    url:'/',
    method:'POST',
    headers: {
      'content-type':'application/json'
    },
    data:JSON.stringify({downloadList})
  })
  .then(response => {
    console.log(response);
  })
}