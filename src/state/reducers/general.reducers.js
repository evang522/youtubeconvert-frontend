//================================== Import Dependencies ====================>
import {SET_SEARCH_TERM, CLEAR_DOWNLOAD_LIST, STORE_VIDEO_LIST, ADD_SONG_TO_LIST} from '../actions/general.actions';



const initialState = {
  loading:false,
  searchTerm:'',
  resultsList:[],
  downloadList:[]
}


export const generalReducer = (state=initialState, action) => {

  switch(action.type) {

    case SET_SEARCH_TERM:
    return {
      ...state,
      searchTerm:action.searchTerm
    }

    //============
    case STORE_VIDEO_LIST:
    return {
      ...state,
      resultsList: action.videoList
    }

    //============

    case ADD_SONG_TO_LIST:
    return {
      ...state,
      downloadList: [
        ...state.downloadList,
        action.song
      ]
    }
    //=============

    case CLEAR_DOWNLOAD_LIST:
    return {
      ...state,
      downloadList:[]
    }


    //=============
    default: 
    return state;


  } 
}


//make accessible for store
export default generalReducer;

