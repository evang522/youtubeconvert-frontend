import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
//================================== Import Dependencies ====================>
//Import Reducers
import generalReducer from '../reducers/general.reducers';


const reducers = combineReducers({
  'general':generalReducer
});

const store = createStore(
  reducers,
  compose(applyMiddleware(thunk))
)

export default store;