import React, { Component } from 'react';
import {Provider} from 'react-redux';
import store from './state/store/store';
import Header from './components/Header';
import QueryInput from './components/QueryInput';
import ResultsContainer from './components/ResultsContainer';
import DownloadList from './components/DownloadList';

//================================== App Component ====================>


class App extends Component {
  render() {
    return (
      <Provider store={store}>
      <div className="App">
        <Header>ClipSplitter</Header>
        <QueryInput />
        <DownloadList/>
        <ResultsContainer/>
      </div>
      </Provider>
    );
  }
}

export default App;
