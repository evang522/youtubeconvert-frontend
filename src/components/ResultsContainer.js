//================================== Import Dependencies ====================>
import React,{Component} from 'react';
import VideoInstance from '../components/VideoInstance';
import {connect} from 'react-redux';
import '../components/styles/ResultsContainer.css';

//================================== ResultsContainer ====================>

export class ResultsContainer extends Component {

  render() {
    const videoResults = this.props.videoResults.length ? this.props.videoResults.map((video,index) => <VideoInstance
      key={index}
      title={video.snippet.title} 
      imgHref={video.snippet.thumbnails.default.url} 
      description={video.snippet.description}
      videoId={video.id.videoId}
      />) : '';

    return (
      <section className='results-container container'>
        <div className='row'>
        {videoResults || ''}
        </div>
      </section>
    )
  }

}

const mapStateToProps = state => ({
  videoResults:state.general.resultsList
})

export default connect(mapStateToProps)(ResultsContainer);