//================================== Import Dependencies ====================>
import React, {Component} from 'react';
import {connect} from 'react-redux';
import '../components/styles/DownloadList.css';
import {requestDownload, clearDownloadLIst} from '../state/actions/general.actions';

//================================== DownloadList ====================>

export class DownloadList extends Component {
  render() {

    const SongToDownload = props => (<div className='btn btn-success col-sm-3 col-md-2 m-1'>{props.songId}</div>)

    const songsToDownload = this.props.downloadList.length ? this.props.downloadList.map((song, index) => <SongToDownload songId={song}/>) : ''

    return (
      <div>
      <section className='download-list-container container'>
        <div className='row'>
        {songsToDownload || ''}
        </div>
      </section>
      {this.props.downloadList.length ? 
      <div className='row mt-3'>
        <button onClick={() => this.props.dispatch(requestDownload())}className="btn btn-secondary col-sm-5 download-button">
         Download
        </button>
        <button onClick={() => this.props.dispatch(clearDownloadLIst())} className='btn btn-warning download-button col-sm-5'>
          Clear List
        </button>
      </div> : ''}
      </div>
    )
  }
}


const mapStateToProps = state => ({
  downloadList:state.general.downloadList
});

export default connect(mapStateToProps)(DownloadList);