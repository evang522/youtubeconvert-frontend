//================================== Import Dependencies ====================>
import React from 'react';
import './styles/Header.css';


//================================== Header Component ====================>

export default props => {
  return (
    <header className='header-component'>
      <h1>{props.children}</h1>
    </header>
  )
}