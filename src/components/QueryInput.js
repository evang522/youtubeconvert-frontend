//================================== Import Dependencies ====================>
import React, {Component} from 'react';
import {setSearchTerm, getVideoResults} from '../state/actions/general.actions';
import {connect} from 'react-redux';
import '../components/styles/QueryInput.css';

//================================== Query-Input Component ====================>

export class QueryInput extends Component {

  onSubmit = (e)  => {
    e.preventDefault();
    this.props.dispatch(getVideoResults())
  }

  render() {
    return (
      <section className='query-input-container'>
        <form onSubmit={this.onSubmit}> 
          <label>Search for A Youtube Video</label>
          <br/>
          <input className='col-sm-8' onChange={e => this.props.dispatch(setSearchTerm(e.target.value))}/>
        </form>
      </section>
    )
  }
}


export default connect()(QueryInput);