//================================== Import Dependencies ====================>
import React,{Component} from 'react';
import '../components/styles/VideoInstance.css';
import {addSongToList} from '../state/actions/general.actions';
import {connect} from 'react-redux';


//================================== VideoInstance ====================>
export class VideoInstance extends Component {


  onClick = (songId) => {
    this.props.dispatch(addSongToList(songId));
  }

  render() {
    return (
      <div className='video-instance-container col-sm-5 col-md-4 m-2 p-4'>
        <div className='vi-title'>
          <a href={`https://www.youtube.com/watch?v=${this.props.videoId}`} target="_blank" title={this.props.title}>{this.props.title ? this.props.title.substring(0,30) + '...' : ''}</a>
        </div>
        <div className='vi-image'>
          <img src={this.props.imgHref} alt={'Video Avatar'}/>
        </div>
        <br/>
        <div className='vi-description' title={this.props.description}>
          {this.props.description.substring(0,110) + '...'}
        </div>
        <button onClick={() => this.onClick(this.props.videoId)} className={this.props.downloadList.includes(this.props.videoId) ? 'btn btn-warning add-button' : 'btn btn-primary add-button'}>
          {this.props.downloadList.includes(this.props.videoId) ?  'Added' : 'Add'}
        </button>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  downloadList:state.general.downloadList
})

export default connect(mapStateToProps)(VideoInstance);